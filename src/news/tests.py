import json
from datetime import datetime

from django.contrib.auth.models import User
from django.test import TestCase
from django.urls import reverse
from rest_framework import status
from rest_framework.authtoken.models import Token
from news.models import Rubric, News, Like, Comment
from rest_framework.test import APIClient


class APIRubricViewSetTest(TestCase):
    def setUp(self):
        self.user = User.objects.create(username="username", password="password")
        self.token = Token.objects.create(user=self.user).key
        self.name = "nametestrubric"
        self.rubric = Rubric.objects.create(name="test_rubric")

    def test_read_rubrics(self):
        client = APIClient()
        client.credentials(HTTP_AUTHORIZATION="Token " + self.token)
        response = client.get(reverse("news:v1:rubric-list"),)

        self.assertEqual(response.status_code, status.HTTP_200_OK)

    def test_read_rubric(self):
        client = APIClient()
        client.credentials(HTTP_AUTHORIZATION="Token " + self.token)
        response = client.get(
            reverse("news:v1:rubric-detail", kwargs={"pk": self.rubric.pk})
        )

        self.assertEqual(response.status_code, status.HTTP_200_OK)


class APINewsViewSet(TestCase):
    def setUp(self):
        self.user = User.objects.create(username="username", password="password")
        self.token = Token.objects.create(user=self.user).key
        self.title = "test_title"
        self.text = "test_text"
        self.rubric = Rubric.objects.create(name="rubric")
        self.news = News.objects.create(
            title=self.title, text=self.text, rubric=self.rubric
        )

    def test_read_news(self):
        client = APIClient()
        client.credentials(HTTP_AUTHORIZATION="Token " + self.token)

        response = client.get(reverse("news:v1:news-list"))

        self.assertEqual(response.status_code, status.HTTP_200_OK)

    def test_read_page_news(self):
        client = APIClient()
        client.credentials(HTTP_AUTHORIZATION="Token " + self.token)

        page_size = 1

        response = client.get(
            f'{ reverse("news:v1:news-list") }?page={1}&page_size={page_size}'
        )

        self.assertEqual(response.status_code, status.HTTP_200_OK)

    def test_read_filter_news(self):
        client = APIClient()
        client.credentials(HTTP_AUTHORIZATION="Token " + self.token)

        date = self.news.pub_date.strftime("%Y-%m-%d")
        response = client.get(
            f'{ reverse("news:v1:news-list") }?rubric={self.rubric.pk}&min_date={date}&max_date={date}'
        )
        results = json.loads(response.content).get("results")
        self.assertEqual(self.news.pk, results[-1].get("id", None))

    def test_read_search_news(self):
        client = APIClient()
        client.credentials(HTTP_AUTHORIZATION="Token " + self.token)

        response = client.get(
            f'{ reverse("news:v1:news-list") }?search={self.news.title}'
        )
        results = json.loads(response.content).get("results")
        self.assertEqual(self.news.pk, results[-1].get("id", None))

    def test_read_detail_news(self):
        client = APIClient()
        client.credentials(HTTP_AUTHORIZATION="Token " + self.token)

        response = client.get(
            reverse("news:v1:news-detail", kwargs={"pk": self.news.pk})
        )

        self.assertEqual(response.status_code, status.HTTP_200_OK)


class APICommentViewSet(TestCase):
    def createComment(self) -> "response":
        client = APIClient()
        client.credentials(HTTP_AUTHORIZATION="Token " + self.token)

        response = client.post(
            reverse("news:v1:comment-list", kwargs={"id": self.news.pk}),
            {"content": self.content},
        )
        return response

    def update_comment(self, token, comment_id, news_id, content) -> "response":
        client = APIClient()
        client.credentials(HTTP_AUTHORIZATION="Token " + token)

        response = client.put(
            reverse("news:v1:comment-detail", kwargs={"id": news_id, "pk": comment_id}),
            {"content": content,},
        )

        return response

    def setUp(self):
        self.user = User.objects.create(username="username", password="password")
        self.token = Token.objects.create(user=self.user).key
        self.rubric = Rubric.objects.create(name="rubric")
        self.news = News.objects.create(title="test", text="text", rubric=self.rubric)
        self.content = "test_comment"

    def test_create_comment(self):
        response = self.createComment()
        pk = json.loads(response.content).get("id")
        comment = Comment.objects.filter(
            pk=pk, user=self.user, news=self.news, content=self.content
        ).first()
        self.assertTrue(comment)

    def test_delete_comment_is_author(self):
        response = self.createComment()
        pk = json.loads(response.content).get("id")

        client = APIClient()
        client.credentials(HTTP_AUTHORIZATION="Token " + self.token)
        response = client.delete(
            reverse("news:v1:comment-detail", kwargs={"id": self.news.pk, "pk": pk})
        )

        comment = Comment.objects.filter(
            pk=pk, user=self.user, news=self.news, content=self.content
        ).first()
        self.assertTrue(not comment)

    def test_update_comment_is_author(self):
        response = self.createComment()
        comment_id = json.loads(response.content).get("id")

        new_content = "new_comment"
        self.update_comment(self.token, comment_id, self.news.pk, new_content)

        comment = Comment.objects.filter(pk=comment_id).first()
        self.assertEqual(comment.content, new_content)

    def test_update_edited_status(self):
        response = self.createComment()
        comment_id = json.loads(response.content).get("id")

        new_content = "new_comment"
        self.update_comment(self.token, comment_id, self.news.pk, new_content)

        comment = Comment.objects.filter(pk=comment_id).first()

        self.assertTrue(comment.edited)

    def test_delete_comment_is_not_author(self):
        response = self.createComment()
        pk = json.loads(response.content).get("id")

        new_user = User.objects.create(username="new_username", password="password")
        new_token = Token.objects.create(user=new_user).key

        client = APIClient()
        client.credentials(HTTP_AUTHORIZATION="Token " + new_token)
        response = client.delete(
            reverse("news:v1:comment-detail", kwargs={"id": self.news.pk, "pk": pk})
        )

        comment = Comment.objects.filter(
            pk=pk, user=self.user, news=self.news, content=self.content
        ).first()

        self.assertTrue(comment)

    def test_update_comment_is_not_author(self):
        response = self.createComment()
        comment_id = json.loads(response.content).get("id")
        content = json.loads(response.content).get("content")

        new_user = User.objects.create(username="new_username", password="password")
        new_token = Token.objects.create(user=new_user).key

        new_content = "new_comment"
        response = self.update_comment(new_token, comment_id, self.news.pk, new_content)

        comment = Comment.objects.filter(pk=comment_id).first()
        self.assertEqual(comment.content, content)


class APILikeViewSet(TestCase):
    def createLike(self) -> "response":
        client = APIClient()
        client.credentials(HTTP_AUTHORIZATION="Token " + self.token)

        pk = self.news.pk

        response = client.post(
            reverse("news:v1:like-list", kwargs={"id": self.news.pk,}),
        )
        return response

    def setUp(self):
        self.user = User.objects.create(username="username", password="password")
        self.token = Token.objects.create(user=self.user).key
        self.news = News.objects.create(title="test", text="text")

    def test_create_like(self):
        response = self.createLike()
        pk = json.loads(response.content).get("id")
        like = Like.objects.filter(pk=pk, user=self.user, news=self.news).first()
        self.assertTrue(like)

    def test_delete_like_is_author(self):
        response = self.createLike()
        like_id = json.loads(response.content).get("id")
        news_pk = self.news.pk

        client = APIClient()
        client.credentials(HTTP_AUTHORIZATION="Token " + self.token)
        response = client.delete(
            reverse("news:v1:like-detail", kwargs={"id": self.news.pk, "pk": like_id})
        )
        self.assertEqual(response.status_code, status.HTTP_204_NO_CONTENT)

        like = Like.objects.filter(pk=like_id, user=self.user, news=self.news).first()
        self.assertTrue(not like)

    def test_delete_like_is_not_author(self):
        response = self.createLike()
        like_id = json.loads(response.content).get("id")
        news_pk = self.news.pk

        new_user = User.objects.create(username="new_username", password="password")
        new_token = Token.objects.create(user=new_user).key

        client = APIClient()
        client.credentials(HTTP_AUTHORIZATION="Token " + new_token)
        response = client.delete(
            reverse("news:v1:like-detail", kwargs={"id": self.news.pk, "pk": like_id})
        )

        like = Like.objects.filter(pk=like_id, user=self.user, news=self.news).first()
        self.assertTrue(like)

    def test_read_likes(self):
        response = self.createLike()
        news_id = self.news.pk

        client = APIClient()
        client.credentials(HTTP_AUTHORIZATION="Token " + self.token)
        response = client.get(
            reverse("news:v1:like-list", kwargs={"id": self.news.pk,})
        )
        self.assertEqual(response.status_code, status.HTTP_200_OK)
