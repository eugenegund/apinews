from django.urls import path, include
from rest_framework.routers import DefaultRouter

from . import views as ver_views


router = DefaultRouter()
router.register("rubrics", ver_views.APIRubricViewSet, basename="rubric")
router.register("news", ver_views.APINewsViewSet, basename="news")

router_actions = DefaultRouter()
router_actions.register("likes", ver_views.APILikeViewSet, basename="like")
router_actions.register("comments", ver_views.APICommentViewSet, basename="comment")

urlpatterns = [
    path("", include(router.urls)),
    path("news/<int:id>/", include(router_actions.urls)),
]
