from django_filters import rest_framework as filters
from ...models import News


class NewsFilter(filters.FilterSet):
    pub_date = filters.BaseRangeFilter(lookup_expr="range")

    class Meta:
        model = News
        fields = ["pub_date", "rubric"]
