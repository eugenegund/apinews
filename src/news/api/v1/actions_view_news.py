from ...models import News


class ActionsViewNews:
    def get_news(self):
        news = News.objects.filter(pk=self.kwargs.get("id", None)).first()
        return news
