from rest_framework import serializers
from ...models import Rubric, News, Comment, Like


class RubricSerializer(serializers.ModelSerializer):
    class Meta:
        model = Rubric
        fields = "__all__"


class NewsSerializer(serializers.ModelSerializer):
    class Meta:
        model = News
        fields = "__all__"


class CommentSerializer(serializers.ModelSerializer):
    def __init__(self, *args, **kwargs):
        self.news = kwargs["context"].get("news", None)
        self.author = kwargs["context"].get("author", None)
        super().__init__(*args, **kwargs)

    def create(self, validated_data):
        return Comment.objects.create(
            user=self.author, news=self.news, **validated_data
        )

    def update(self, instance, validated_data):
        instance.content = validated_data.get("content", instance.content)
        instance.edited = True
        instance.save()

        return instance

    class Meta:
        model = Comment
        read_only_fields = (
            "user",
            "news",
            "edited",
        )
        fields = "__all__"


class LikeSerializer(serializers.ModelSerializer):
    def __init__(self, *args, **kwargs):
        self.user = kwargs["context"].get("user", None)
        self.news = kwargs["context"].get("news", None)
        super().__init__(*args, **kwargs)

    def create(self, validated_data):
        return Like.objects.create(user=self.user, news=self.news)

    class Meta:
        model = Like
        read_only_fields = ("user", "news")
        fields = "__all__"
