from .actions_view_news import ActionsViewNews
from .filters import NewsFilter
from django_filters.rest_framework import DjangoFilterBackend
from rest_framework import filters
from rest_framework.authentication import TokenAuthentication
from rest_framework.mixins import (
    DestroyModelMixin,
    CreateModelMixin,
    RetrieveModelMixin,
    ListModelMixin,
)
from .paginator import CustomPageNumberPagination
from rest_framework.permissions import IsAuthenticated
from rest_framework.viewsets import ModelViewSet, GenericViewSet, ReadOnlyModelViewSet
from ...models import Rubric, News, Comment, Like
from ...permissions import IsUserOrReadOnlyActionsNews
from .serializers import (
    RubricSerializer,
    NewsSerializer,
    CommentSerializer,
    LikeSerializer,
)


class APIRubricViewSet(ReadOnlyModelViewSet):
    permission_classes = (IsAuthenticated,)
    authentication_classes = (TokenAuthentication,)

    queryset = Rubric.objects.all()
    serializer_class = RubricSerializer


class APINewsViewSet(ReadOnlyModelViewSet):
    permission_classes = (IsAuthenticated,)
    authentication_classes = (TokenAuthentication,)

    pagination_class = CustomPageNumberPagination

    queryset = News.objects.all()
    serializer_class = NewsSerializer

    filter_backends = (DjangoFilterBackend, filters.SearchFilter)
    filterset_class = NewsFilter
    search_fields = ["title"]


class APICommentViewSet(ModelViewSet, ActionsViewNews):
    permission_classes = (IsAuthenticated, IsUserOrReadOnlyActionsNews)
    authentication_classes = (TokenAuthentication,)

    queryset = Comment.objects.all()
    serializer_class = CommentSerializer

    def get_serializer_context(self):
        context = super().get_serializer_context()
        context.setdefault("author", self.request.user)
        context.setdefault("news", self.get_news())
        return context

    def get_queryset(self):
        news = self.get_news()
        if news is None:
            return self.queryset.none()
        return news.comment_set


class APILikeViewSet(
    CreateModelMixin,
    DestroyModelMixin,
    ListModelMixin,
    RetrieveModelMixin,
    GenericViewSet,
    ActionsViewNews,
):
    permission_classes = (IsAuthenticated, IsUserOrReadOnlyActionsNews)
    authentication_classes = (TokenAuthentication,)

    queryset = Like.objects.all()
    serializer_class = LikeSerializer

    def get_serializer_context(self):
        context = super().get_serializer_context()
        context.setdefault("user", self.request.user)
        context.setdefault("news", self.get_news())
        return context

    def get_queryset(self):
        news = self.get_news()
        if news is None:
            return self.queryset.none()
        return news.like_set
