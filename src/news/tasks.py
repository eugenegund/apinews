from celery import shared_task
from .rabbit import send_message


@shared_task
def send_penzanews_task():
    from .models import News

    host = ""
    href = ""

    end_news = News.objects.filter(host="https://penzanews.ru/").order_by("-pub_date").first()
    if end_news:
        host = end_news.host
        href = end_news.href

    send_message({"host": host, "href": href}, "penzanews")


@shared_task
def news_save_db(news):
    from .models import News, Rubric

    print(f"SAVE_NEWS: {news}")

    rubric = Rubric.objects.get_or_create(name=news.get("rubric", ""))
    news["rubric"] = rubric[0]
    News.objects.create(**news)
