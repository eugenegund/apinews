from django.urls import path, include

import news.api.v1.urls as v1_urls

urlpatterns = [
    path("v1/", include((v1_urls, "news"), namespace="v1")),
]
