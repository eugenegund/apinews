from django.contrib.auth.models import User
from django.db import models


class Rubric(models.Model):
    name = models.CharField(
        max_length=20, db_index=True, verbose_name="Название", unique=True
    )

    def __str__(self):
        return self.name


class News(models.Model):
    title = models.CharField(max_length=150, verbose_name="Название")
    text = models.TextField(default="", verbose_name="Новость")
    rubric = models.ForeignKey(
        Rubric, null=True, on_delete=models.PROTECT, verbose_name="Рубрика"
    )
    pub_date = models.DateTimeField(
        auto_now_add=True, db_index=True, verbose_name="Дата добавления"
    )
    host = models.CharField(
        max_length=100, verbose_name="Источник", default="https://penzanews.ru/"
    )
    href = models.CharField(
        max_length=100, verbose_name="Ссылка на новость", default="/society/142901-2020"
    )

    @property
    def comments_count(self):
        return self.comment_set.count()

    def __str__(self):
        return f"title: {self.title}"


class Comment(models.Model):
    news = models.ForeignKey(News, on_delete=models.CASCADE, verbose_name="Новость")
    user = models.CharField(max_length=30, verbose_name="Автор")
    content = models.TextField(verbose_name="Комментарий")
    edited = models.BooleanField(default=False, verbose_name="Редактирован")
    created_date = models.DateTimeField(
        auto_now_add=True, db_index=True, verbose_name="Дата создания"
    )


class Like(models.Model):
    news = models.ForeignKey("News", on_delete=models.CASCADE, verbose_name="Новость")
    user = models.ForeignKey(
        User, on_delete=models.CASCADE, verbose_name="Пользователь"
    )

    class Meta:
        unique_together = ("news", "user")
