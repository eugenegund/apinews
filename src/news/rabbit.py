from kombu import Connection, Producer
from apinews.settings import CELERY_BROKER_URL


def send_message(mess: dict, queue: str) -> None:
    with Connection(CELERY_BROKER_URL) as conn:
        with conn.channel() as channel:
            producer = Producer(channel)
            producer.publish(
                mess, retry=True, routing_key=queue
            )
