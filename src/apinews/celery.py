from __future__ import absolute_import, unicode_literals
from kombu import Connection
import os
from celery import Celery
from apinews.settings import CELERY_BROKER_URL
from apinews.queues import penzanews_queue, news_queue
from apinews.consumer_steps import SaveNewsConsumerStep


# set the default Django settings module for the 'celery' program.
os.environ.setdefault("DJANGO_SETTINGS_MODULE", "apinews.settings")

app = Celery("apinews")

# Using a string here means the worker doesn't have to serialize
# the configuration object to child processes.
# - namespace='CELERY' means all celery-related configuration keys
#   should have a `CELERY_` prefix.
app.config_from_object("django.conf:settings", namespace="CELERY")

# Load task modules from all registered Django app configs.
app.autodiscover_tasks()


with Connection(CELERY_BROKER_URL) as conn:
    with conn.channel() as channel:
        penzanews_queue.declare(channel=channel)
        news_queue.declare(channel=channel)

app.steps["consumer"].add(SaveNewsConsumerStep)
