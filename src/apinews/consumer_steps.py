import json
from celery import bootsteps
from kombu import Consumer
from apinews.queues import news_queue
from news.tasks import news_save_db


class SaveNewsConsumerStep(bootsteps.ConsumerStep):
    def get_consumers(self, channel):
        return [
            Consumer(
                channel,
                queues=[news_queue],
                callbacks=[self.save_news],
                accept=["json"],
            )
        ]

    def save_news(self, body, message):
        print("Received message: {0!r}".format(body))
        news = json.loads(message.body)
        news_save_db.delay(news)
        message.ack()


