from django.urls import path, include

urlpatterns = [
    path("rest-auth/", include(("rest_auth.urls", "rest_auth"), namespace="rest_auth")),
    path(
        "rest-auth/registration/",
        include(("rest_auth.registration.urls", "rest_auth"), namespace="registration"),
    ),
]
