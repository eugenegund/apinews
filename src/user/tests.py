import json
from django.test import TestCase
from rest_framework import status
from rest_framework.reverse import reverse
from rest_framework.test import APIClient


class APIUserCreate(TestCase):
    def test_registration(self):

        response = self.client.post(
            reverse("user:registration:rest_register"),
            data={
                "username": "user",
                "password1": "passtesttest",
                "password2": "passtesttest",
            },
            content_type="application/json",
        )
        self.assertEqual(response.status_code, status.HTTP_201_CREATED)


class APIAuthView(TestCase):
    def setUp(self):
        response = self.client.post(
            reverse("user:registration:rest_register"),
            data={
                "username": "user",
                "password1": "passtesttest",
                "password2": "passtesttest",
            },
            content_type="application/json",
        )

    def test_login(self):
        response = self.client.post(
            reverse("user:rest_auth:rest_login"),
            data={"username": "user", "password": "passtesttest",},
            content_type="application/json",
        )
        self.assertEqual(response.status_code, status.HTTP_200_OK)

    def test_logout(self):
        response = self.client.post(
            reverse("user:rest_auth:rest_login"),
            data={"username": "user", "password": "passtesttest",},
            content_type="application/json",
        )
        self.token = json.loads(response.content).get("key")

        client = APIClient()
        client.credentials(HTTP_AUTHORIZATION="Token " + self.token)
        response = client.post(reverse("user:rest_auth:rest_logout"))

        self.assertEqual(response.status_code, status.HTTP_200_OK)
