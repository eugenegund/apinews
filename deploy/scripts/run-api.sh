#!/bin/bash

python /apinews/src/manage.py migrate
python /apinews/src/manage.py collectstatic --no-input --clear
cd /apinews/src/
gunicorn apinews.wsgi:application -w 2 --bind 0.0.0.0:8000